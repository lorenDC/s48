// Mock DB
let posts = [];

let count = 1

document.querySelector("#form-add-post").addEventListener("submit", (e) => {
	e.preventDefault()
	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	})
	count++
	console.log(posts)
	alert('Post added!')
	showPosts();
})

const showPosts = () => {
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
				
    	<div id="post-${post.id}">
    		<h3 id="post-title-${post.id}">${post.title}</h3>
    		<p id="post-body-${post.id}">${post.body}</p>
	    	<button onClick="editPost('${post.id}')">Edit</button>
			<button onClick="deletePost('${post.id}')">Delete</button>

    	</div>

	`
	})
	console.log(postEntries)
	document.querySelector("#div-post-entries").innerHTML = postEntries
}


const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector("#txt-edit-id").value = id
	document.querySelector("#txt-edit-title").value = title
	document.querySelector("#txt-edit-body").value = body
}

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
	e.preventDefault()
	for(i=0; i < posts.length; i++){
		if(posts[i].id.toString() == document.querySelector("#txt-edit-id").value){
			posts[i].title = document.querySelector("#txt-edit-title").value
			posts[i].body = document.querySelector("#txt-edit-body").value

			showPosts(posts)
			alert("Posts updated!")
			break;
		}
	}
})

const deletePost = (id) => {
	posts.splice(-1);
	let deletedPost = document.querySelector(`#post-${id}`)
	deletedPost.remove()
}